package com.kraftwerking.spring.test;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;

public class App {

	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("com/kraftwerking/spring/test/beans/beans.xml");
		
		OffersDAO offersDAO = (OffersDAO)context.getBean("offersDAO");
				
		try{
			Offer offer1 = new Offer("dave@kraftwerking.com", "coding java", "Dave");
			Offer offer2 = new Offer("karen@kraftwerking.com", "testing software to order", "Karen");
			
			if(offersDAO.create(offer1)){
				System.out.println("offer 1 created");
			}
			
			if(offersDAO.create(offer2)){
				System.out.println("offer 2 created");
			}
			
			Offer offer3 = new Offer(19, "claire@kraftwerking.com", "Web design to fit any budget", "Claire");
			
			if(offersDAO.create(offer3)){
				System.out.println("offer 3 created");
			} else {
				System.out.println("Cannot create object 3");
			}
			
			if(offersDAO.update(offer3)){
				System.out.println("offer 3 updated");
			} else {
				System.out.println("Cannot update object");
			}
			
			offersDAO.delete(80);
			
			List<Offer> offers = offersDAO.getOffers();

			for(Offer offer: offers){
				System.out.println(offer);
			}
			
			Offer offer = offersDAO.getOffer(2);
			
			System.out.println("Should be Mike");
			
			List<Offer> offers4 = new ArrayList<Offer>();
			offers4.add(new Offer("dave@kraftwerking.com", "Cash for programming", "Dave"));
			offers4.add(new Offer("karen@kraftwerking.com", "Elegant web design", "Karen"));
			
			int[] rvals = offersDAO.create(offers4);
			for (int val : rvals){
				System.out.println("Updated " + val + " rows.");
			}
			
			
		} catch (CannotGetJdbcConnectionException ex) {
			System.out.println(ex.getMessage());
			System.out.println(ex.getClass());
		
		} catch (DataAccessException ex) {
			System.out.println(ex.getMessage());
			System.out.println(ex.getClass());
		}
		

		((ClassPathXmlApplicationContext)context).close();
	}

}
