package com.kraftwerking.spring.test;

public class Offer {
	private int id;
	private String email;
	private String text;
	private String name;
	
	public Offer() {
	}
	
	public Offer(String email, String text, String name) {
		super();
		this.email = email;
		this.text = text;
		this.name = name;
	}
	
	public Offer(int id, String email, String text, String name) {
		super();
		this.id = id;
		this.email = email;
		this.text = text;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "Offer [id=" + id + ", email=" + email + ", text=" + text
				+ ", name=" + name + "]";
	}
	
	
}
